## ---------------------------
##
## Script name: example_Irvine_method.R
##
## Purpose of script: Script to provide examples of how to use the Irvine method for KM analysis
##
## Author: Dr. Edward Green
##
## Date Created: 2020-03-29
##
## Copyright (c) Edward Green, 2020
## Email: e.green@dkfz.de
##
## ---------------------------
##
## Notes: 
##   
## The Irvine method facilitates extracting summary statistics, namely the hazard ratio and its variance, 
## as well as missing censor values from KM plots when this information is not provided in the primary article .
## 
## The Irvine method code requires a P value associated with the KM plot to reverse-engineer the KM survival 
## table to then create estimates of the summary statistics.
## 
## The Irvine method is based on non-linear optimisation.
## ---------------------------



#Source the libraries and Irvine analysis functions ---------------------------------
if (!require(nloptr, quietly = TRUE)) install.packages("nloptr")
source('IrvineKM_v0.0.1.R')

#Example of usage in R ---------------------------------

#Data derived from Figure 1a, Hanley et al 2018 (doi.org/10.1093/jnci/djx121)
Hanley_years          <- c(0.00, 0.13, 0.30, 0.49, 0.71, 0.97, 1.26, 1.85, 2.00, 2.63, 3.36, 3.63,3.96, 4.27, 4.68, 5.37, 6.74, 7.41, 8.02, 9.80)
Hanley_control_p      <- c(100, 97.32, 92.61, 92.61, 87.26, 83.75, 79.22, 78.58, 78.58, 77.56, 73.41, 73.41, 70.91, 69.92, 69.92, 69.92, 66.90, 66.90, 66.90, 66.90)
Hanley_experiment_p   <- c(100, 96.40, 94.04, 90.86, 84.00, 77.84, 75.55, 69.46, 68.70, 67.11, 59.00, 56.30, 56.30, 55.06, 55.06, 53.53, 51.34, 51.34, 46.17, 39.70) 
Hanley_control_n      <- 126
Hanley_experiment_n   <- 146
Hanley_p              <- 0.013
Hanley_chi            <- qchisq(Hanley_p, df = 1, lower.tail=FALSE)

#calculate result
Result <- Irvine_method(time_vector              = Hanley_years, 
                        prob_c                   = Hanley_control_p,
                        prob_d                   = Hanley_experiment_p,
                        risk_t0_c                = Hanley_control_n,
                        risk_t0_d                = Hanley_experiment_n,
                        chi_sq_value_to_optimise = Hanley_chi,
                        p_exact                  = TRUE)

#Retrieve Hazard Ratio from results
Result[["hazard_ratio_values"]][["HR"]]



#Example of reading data from Excel document into R then processing ---------------------------------
if (!require(readxl, quietly = TRUE)) install.packages("readxl")
Excel_Input <- readxl::read_excel('shiny/data/example_survival_data.xlsx')

#calculate result
Result <- Irvine_method(time_vector              = Excel_Input[[1]],
                        prob_c                   = Excel_Input[[2]],
                        prob_d                   = Excel_Input[[3]],
                        risk_t0_c                = Hanley_control_n,
                        risk_t0_d                = Hanley_experiment_n,
                        chi_sq_value_to_optimise = Hanley_chi,
                        p_exact                  = TRUE)
