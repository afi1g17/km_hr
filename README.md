# KM_HR

### Introduction
This repository hosts the 'R' source code for the Irvine method of extracting hazard ratios from Kaplan-Meier plot coordinates. 

### Installation
The easiest way to use this tool is to use the web-based Rshiny app available through shinyapps.io at:
https://edgreen21.shinyapps.io/km_hr/

For local installation, clone or download the repository. 

### Problems
If you have problems with the code, the best way to reach us is by raising an issue here in GitLab. You can also contact the authors, but email may well end up in spam:
Andrew Irvine, (afirvine at symbol gmail.com)
Edward Green, (e.green at symbol dkfz.de)

### References
paper currently under review
